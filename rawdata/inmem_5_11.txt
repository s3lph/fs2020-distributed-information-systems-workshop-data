[OVERALL], RunTime(ms), 1173
[OVERALL], Throughput(ops/sec), 852.5149190110827
[TOTAL_GCS_G1_Young_Generation], Count, 0
[TOTAL_GC_TIME_G1_Young_Generation], Time(ms), 0
[TOTAL_GC_TIME_%_G1_Young_Generation], Time(%), 0.0
[TOTAL_GCS_G1_Old_Generation], Count, 0
[TOTAL_GC_TIME_G1_Old_Generation], Time(ms), 0
[TOTAL_GC_TIME_%_G1_Old_Generation], Time(%), 0.0
[TOTAL_GCs], Count, 0
[TOTAL_GC_TIME], Time(ms), 0
[TOTAL_GC_TIME_%], Time(%), 0.0
[READ], Operations, 518
[READ], AverageLatency(us), 664.3976833976834
[READ], MinLatency(us), 194
[READ], MaxLatency(us), 2089
[READ], 95thPercentileLatency(us), 1230
[READ], 99thPercentileLatency(us), 1251
[READ], Return=OK, 518
[CLEANUP], Operations, 1
[CLEANUP], AverageLatency(us), 340.0
[CLEANUP], MinLatency(us), 340
[CLEANUP], MaxLatency(us), 340
[CLEANUP], 95thPercentileLatency(us), 340
[CLEANUP], 99thPercentileLatency(us), 340
[UPDATE], Operations, 482
[UPDATE], AverageLatency(us), 1549.377593360996
[UPDATE], MinLatency(us), 954
[UPDATE], MaxLatency(us), 10175
[UPDATE], 95thPercentileLatency(us), 2305
[UPDATE], 99thPercentileLatency(us), 2505
[UPDATE], Return=OK, 482
