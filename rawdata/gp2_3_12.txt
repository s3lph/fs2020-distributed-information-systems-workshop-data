[OVERALL], RunTime(ms), 1085
[OVERALL], Throughput(ops/sec), 921.6589861751152
[TOTAL_GCS_G1_Young_Generation], Count, 0
[TOTAL_GC_TIME_G1_Young_Generation], Time(ms), 0
[TOTAL_GC_TIME_%_G1_Young_Generation], Time(%), 0.0
[TOTAL_GCS_G1_Old_Generation], Count, 0
[TOTAL_GC_TIME_G1_Old_Generation], Time(ms), 0
[TOTAL_GC_TIME_%_G1_Old_Generation], Time(%), 0.0
[TOTAL_GCs], Count, 0
[TOTAL_GC_TIME], Time(ms), 0
[TOTAL_GC_TIME_%], Time(%), 0.0
[READ], Operations, 496
[READ], AverageLatency(us), 645.9072580645161
[READ], MinLatency(us), 163
[READ], MaxLatency(us), 1363
[READ], 95thPercentileLatency(us), 1229
[READ], 99thPercentileLatency(us), 1281
[READ], Return=OK, 496
[CLEANUP], Operations, 1
[CLEANUP], AverageLatency(us), 282.0
[CLEANUP], MinLatency(us), 282
[CLEANUP], MaxLatency(us), 282
[CLEANUP], 95thPercentileLatency(us), 282
[CLEANUP], 99thPercentileLatency(us), 282
[UPDATE], Operations, 504
[UPDATE], AverageLatency(us), 1355.5039682539682
[UPDATE], MinLatency(us), 662
[UPDATE], MaxLatency(us), 11927
[UPDATE], 95thPercentileLatency(us), 2179
[UPDATE], 99thPercentileLatency(us), 2479
[UPDATE], Return=OK, 504
