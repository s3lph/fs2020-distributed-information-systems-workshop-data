#!/usr/bin/env python3

import os

from statistics import mean, stdev
from matplotlib import pyplot as plt


NAMES = [f'rawdata/{sb}_{j}_{i:02}.txt' for i in range(1,21) for j in range(1,6) for sb in ['inmem', 'gp2', 'gp2mem']]


TOKENMAP = {
    '[READ]': 'read',
    '[CLEANUP]': 'cleanup',
    '[UPDATE]': 'write',

    'Operations': 'N',
    'AverageLatency(us)': 'mean',
    'MinLatency(us)': 'min',
    'MaxLatency(us)': 'max',
    '95thPercentileLatency(us)': 'p95',
    '99thPercentileLatency(us)': 'p99'
}


def parse_ycsb_file(name: str):
    basename = os.path.basename(name).split('.')[0]
    backend, replication, run = basename.split('_')
    replication = int(replication)
    run = int(run)
    
    parsed = {}
    with open(name, 'r') as f:
        lines = f.readlines()
    for line in lines:
        tokens = line.split(',')
        operation = TOKENMAP.get(tokens[0].strip(), None)
        metric = TOKENMAP.get(tokens[1].strip(), None)
        if not operation or not metric:
            continue
        parsed.setdefault(operation, {})[metric] = float(tokens[2].strip())
    return backend, replication, run, parsed


def val(op, metric, backend, *, fn):
    return [fn([r[op][metric] for r in run]) for run in PARSED[backend].values()]


def barplot(p, op, metric):
    p.bar([x-0.2 for x in range(1,6)], height=val(op, metric, 'gp2', fn=mean), yerr=val(op, metric, 'gp2', fn=stdev), ecolor='red', capsize=3, width=0.2)
    p.bar([x+0.0 for x in range(1,6)], height=val(op, metric, 'inmem', fn=mean), yerr=val(op, metric, 'inmem', fn=stdev), ecolor='red', capsize=3, width=0.2)
    p.bar([x+0.2 for x in range(1,6)], height=val(op, metric, 'gp2mem', fn=mean), yerr=val(op, metric, 'gp2mem', fn=stdev), ecolor='red', capsize=3, width=0.2)
    p.legend(['GP2', 'memory only', 'memory, GP2 backed'], loc='lower right')


PARSED = {}
for name in NAMES:
    backend, replication, run, parsed = parse_ycsb_file(name)
    PARSED.setdefault(backend, {}).setdefault(replication, []).append(parsed)


plt.title('YCSB Workload A: Aerospike Write Latency')
wavg = plt.subplot(131, title='Mean Write Latency, 20 runs', xlabel='Replication Factor', ylabel='Write latency / µs')
barplot(wavg, 'write', 'mean')
wp95 = plt.subplot(132, title='99th Percentile Write Latency, 20 runs', xlabel='Replication Factor', ylabel='Write latency / µs')
barplot(wp95, 'write', 'p99')
wmin = plt.subplot(133, title='Minimal Write Latency, 20 runs', xlabel='Replication Factor', ylabel='Write latency / µs')
barplot(wmin, 'write', 'min')
wfig = plt.gcf()
wfig.set_size_inches(15, 6)
wfig.savefig('plots/latency_write.png')


plt.title('YCSB Workload A: Aerospike Read Latency')
ravg = plt.subplot(131, title='Mean Read Latency, 20 runs', xlabel='Replication Factor', ylabel='Read latency / µs')
barplot(ravg, 'read', 'mean')
rp95 = plt.subplot(132, title='99th Percentile Read Latency, 20 runs', xlabel='Replication Factor', ylabel='Read latency / µs')
barplot(rp95, 'read', 'p99')
rmin = plt.subplot(133, title='Minimal Read Latency, 20 runs', xlabel='Replication Factor', ylabel='Read latency / µs')
barplot(rmin, 'read', 'min')
rfig = plt.gcf()
rfig.set_size_inches(15, 6)
rfig.savefig('plots/latency_read.png')
