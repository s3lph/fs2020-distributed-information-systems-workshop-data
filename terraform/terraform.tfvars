
# SET YOUR PUBKEY HERE
pubkey = "ssh-rsa ... you@example.com"

# CONFIGURE SERVER PARAMETERS
server_count = 5
server_instance_type = "m5.xlarge"
server_volume_type = "gp2"
server_volume_size = 50

# CONFIGURE CLIENT PARAMETERS
client_instance_type = "m5.xlarge"


# HOTFIXES

# us-east-1e does not have m5.xlarge VMs
aws_az_blacklist = ["us-east-1e"]
