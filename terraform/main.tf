
provider "aws" {
  version = "~> 2.0"
  region  = "us-east-1"
}


### VARIABLES

variable "pubkey" {
  type = string
}

variable "server_count" {
  type    = number
  default = 3
}

variable "server_instance_type" {
  type = string
}

variable "server_volume_type" {
  type    = string
  default = "gp2"
}

variable "server_volume_size" {
  type    = number
  default = 10
}

variable "image_id" {
  type    = string
  default = "ami-07ebfd5b3428b6f4d"
}

variable "client_instance_type" {
  type = string
}

variable "aws_az_blacklist" {
  type = list
  default = []
}


### GLOBAL SETUP

data "aws_availability_zones" "az" {

}

locals {

  # Blacklist AZs that do not offer the required VM instance type
  aws_availability_zones = [
    for az in data.aws_availability_zones.az.names:
    az if !contains(var.aws_az_blacklist, az)
  ]

}

resource "aws_key_pair" "ssh" {
  key_name   = "aerospike"
  public_key = var.pubkey

  tags = {
    Name = "Aerospike"
  }
}

resource "aws_vpc" "aerospike" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name = "Aerospike"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.aerospike.id

  tags = {
    Name = "Aerospike"
  }
}

resource "aws_route" "default" {
  route_table_id         = aws_vpc.aerospike.default_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.gw.id
}

### CLIENT

resource "aws_subnet" "client" {
  vpc_id                  = aws_vpc.aerospike.id
  cidr_block              = "10.0.0.0/24"
  availability_zone       = local.aws_availability_zones[0]
  map_public_ip_on_launch = true

  tags = {
    Name = "Aerospike Client Subnet"
  }
}

resource "aws_route_table_association" "client" {
  subnet_id      = aws_subnet.client.id
  route_table_id = aws_vpc.aerospike.default_route_table_id

}

resource "aws_security_group" "client" {
  name   = "allow_ssh"
  vpc_id = aws_vpc.aerospike.id

  ingress {
    description = "Allow SSH from anywhere"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "Allow ICMP from anywhere"
    from_port   = 0
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Aerospike Client Subnet"
  }
}

resource "aws_network_interface" "client" {
  subnet_id       = aws_subnet.client.id
  private_ips     = ["10.0.0.10"]
  security_groups = [aws_security_group.client.id]

  tags = {
    Name = "Aerospike Client"
  }
}

resource "aws_instance" "client" {
  instance_type          = var.client_instance_type
  ami                    = var.image_id
  key_name               = aws_key_pair.ssh.key_name

  network_interface {
    network_interface_id = aws_network_interface.client.id
    device_index         = 0
  }

  tags = {
    Name = "Aerospike Client"
  }
}


### SERVERS

resource "aws_security_group" "server" {
  name   = "allow_client"
  vpc_id = aws_vpc.aerospike.id

  ingress {
    description = "Allow SSH from everywhere"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "Allow TCP from client"
    from_port   = 1
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.aerospike.cidr_block]
  }
  ingress {
    description = "Allow UDP from client"
    from_port   = 1
    to_port     = 65535
    protocol    = "udp"
    cidr_blocks = [aws_vpc.aerospike.cidr_block]
 }
  ingress {
    description = "Allow Multicast UDP"
    from_port   = 1
    to_port     = 65535
    protocol    = "udp"
    cidr_blocks = ["239.0.0.0/8"]
 }
 ingress {
    description = "Allow ICMP from client"
    from_port   = 0
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = [aws_vpc.aerospike.cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Aerospike Server"
  }
}

resource "aws_subnet" "server" {
  count                   = var.server_count

  vpc_id                  = aws_vpc.aerospike.id
  cidr_block              = "10.0.${count.index + 1}.0/24"
  availability_zone       = local.aws_availability_zones[count.index % length(local.aws_availability_zones)]
  map_public_ip_on_launch = true
  tags = {
    Name = "Aerospike Server Subnet ${count.index + 1}"
  }
}

resource "aws_route_table_association" "server" {
  count          = var.server_count

  subnet_id      = aws_subnet.server[count.index].id
  route_table_id = aws_vpc.aerospike.default_route_table_id

}

resource "aws_network_interface" "server" {
  count           = var.server_count

  subnet_id       = aws_subnet.server[count.index].id
  private_ips     = ["10.0.${count.index + 1}.10"]
  security_groups = [aws_security_group.server.id]

  tags = {
    Name = "Aerospike Server ${count.index + 1}"
  }
}

resource "aws_instance" "server" {
  count                  = var.server_count

  instance_type          = var.server_instance_type
  ami                    = var.image_id
  key_name               = aws_key_pair.ssh.key_name

  root_block_device  {
    volume_type = var.server_volume_type
    volume_size = var.server_volume_size
  }

  network_interface {
    network_interface_id = aws_network_interface.server[count.index].id
    device_index         = 0
  }

  tags = {
    Name = "Aerospike Server ${count.index + 1}"
  }
}


### OUTPUTS

output "client_ip" {
  value = aws_instance.client.public_ip
}

output "client_dns" {
  value = aws_instance.client.public_dns
}

output "server_ip" {
  value = aws_instance.server.*.public_ip
}

output "server_dns" {
  value = aws_instance.server.*.public_dns
}

output "server_private_dns" {
  value = aws_instance.server.*.private_dns
}
