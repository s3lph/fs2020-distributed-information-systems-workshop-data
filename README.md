# Aerospike - Benchmark Setup and Raw Data

## What is this?

This repository contains our setup for deploying an Aerospike cluster
and YCSB on AWS EC2, the raw data resulting from the YCSB runs, as
well as the code used to extract information from this data.

This benchmark was done as part of the *Distributed Information
Systems* lecture in spring 2020 as part of the MSc Computer Science
curriculum at University of Basel.

**This repository serves as an archive and is not actively maintained.**

## Overview

* `./terraform/` contains the Terraform code and variables used to
    provision the AWS EC2 infrastructure on which the benchmark was
    run.
* `./ansible/` contains the Ansible code and variables used to
    install and configure Aerospike and YCSB.
* `./rawdata/` contains the raw output from YCSB.
* `./eval/` contains the code used for extracting information and
    generating plots out of the raw YCSB output.

## Reproduction

If you want to reproduce our findings, follow the sections below to
get this deployment up and running.

### 1. Infrastructure Provisioning

1. Make your AWS credentials available to Terraform, e.g. though
    environment variables or through `~/.aws/credentials`.
2. `cd terraform`
3. Configure `terraform.tfvars`
4. `terraform apply`

### 2. Installation & Configuration

1. Copy the servers and client hostnames from Terraform's output
2. `cd ansible`
3. Edit `inventory`, paste servers and client hostnames
4. Configure `group_vars/all.yml` as documented in the file
5. `ansible-playbook -i inventory playbook.yml`

### 3. Running a Benchmark

1. `ssh ubuntu@<client-hostname>`
2. `sudo -i`
3. `cd ycsb`
4. `mkdir out`
5. Prepare benchmark: `./bin/ycsb load aerospike -s -P workloads/workloada -p as.host=10.0.1.10`
6. Run benchmark 20 times: `for i in {01..20}; do ./bin/ycsb run aerospike -s -P workloads/workloada -p as.host=10.0.1.10 > out/<base_name>_$i; done`

### 4. More Benchmarking

Repeat 2.4+2.5 and 3.* for all configurations you want to benchmark.

### 5. Results & Teardown

1. On the client: `cp -r /root/ycsb/out /home/ubuntu/out`
2. Locally: `scp -r <client hostname>:out/ out/`
2. `cd terraform`
3. `terraform destroy`